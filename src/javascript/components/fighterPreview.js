
import { createElement } from '../helpers/domHelper';
import { fightersDetails } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___root ' + positionClassName
  });
  if(fighter) {
    const fighterName = createElement({ tagName: 'h4', className: 'fighter-preview-name' });
    const image = createFighterImage(fighter);
    const fighterData = createElement({
      tagName: 'div',
      className: 'fighter-data'
    });
    fighterData.innerHTML = `
      <div class="fighter-characteristics">
        <p>&#9876;&#65039; ${fighter.attack}</p>
      </div>
      <div class="fighter-characteristics">
        <p> &#128737;&#65039; ${fighter.defense}</p>
      </div>
      <div class="fighter-characteristics">
        <p>&#10084;&#65039; ${fighter.health}</p>
      </div>`;

    fighterElement.innerText = fighter.name;
    fighterElement.append(image, fighterName, fighterData);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
