import { controls } from '../../constants/controls';
import { renderArena } from './arena';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    
    let {health:firstInitialHealth} = firstFighter;
    let {health:secondInitialHealth} = secondFighter;
    let critPoints = [1, 1];
    let pressed = new Set();

    document.addEventListener('keydown', function(event) {
      if (event.repeat) {
        return;
      }
      pressed.add(event.code);
      dealDamageToFighter(firstFighter, secondFighter, pressed, critPoints);
      if (critPoints[0] == 0) {
        setTimeout(refreshFirstFighterCrit, 10000, critPoints);
      }
      if (critPoints[1] == 0) {
        setTimeout(refreshSecondFighterCrit, 10000, critPoints);
      }
      healthBarIncrease(secondFighter, secondInitialHealth, 'right');
      healthBarIncrease(firstFighter, firstInitialHealth, 'left');
      if (secondFighter.health <=0) {
        resolve(firstFighter);
      }
      else if (firstFighter.health <=0) {
        resolve(secondFighter);
      }

    });

    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
      return;
    });

      })
}

function refreshFirstFighterCrit(critPoints) {
  critPoints[0] = 1;
}

function refreshSecondFighterCrit(critPoints) {
  critPoints[1] = 1;
}

function dealDamageToFighter(firstFighter, secondFighter, pressedKeys, critPoints) {
  if (pressedKeys.values().next().value == controls.PlayerTwoBlock && pressedKeys.has(controls.PlayerOneAttack)) {
    secondFighter.health -= 0;

  }
  else if (pressedKeys.values().next().value == controls.PlayerOneBlock && pressedKeys.has(controls.PlayerTwoAttack)) {
    firstFighter.health -=  0;

  }
  else if (pressedKeys.values().next().value == controls.PlayerOneAttack && !pressedKeys.has(controls.PlayerOneBlock)) {
    secondFighter.health -= getDamage(firstFighter, secondFighter);

  }
  else if (pressedKeys.values().next().value == controls.PlayerTwoAttack && !pressedKeys.has(controls.PlayerOneBlock)) {
    firstFighter.health -=  getDamage(secondFighter, firstFighter);

  }
  else if (controls.PlayerOneCriticalHitCombination.every(code => pressedKeys.has(code)) && critPoints[0] == 1) {
    secondFighter.health -= firstFighter.attack * 2;
    critPoints[0]-=1;
  }
  else if (controls.PlayerTwoCriticalHitCombination.every(code => pressedKeys.has(code)) && critPoints[1] == 1) {
    firstFighter.health -= secondFighter.attack * 2;
    critPoints[1]-=1;
  }
}

function healthBarIncrease(fighter, firstHealth, position) {
  let barPercent = 100 - (100 - ((fighter.health * 100)/firstHealth));
  if (barPercent < 0) {
    barPercent = 0;
  }
  document.getElementById(position+'-fighter-indicator').style.width = barPercent+'%';
  return barPercent;
}

export function getDamage(attacker, defender) {
  let damage;
  damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    damage = 0;
  }

  return damage;
}

export function getHitPower(fighter) {
  let crit = (Math.random() * (1)) + 1;
  let power = fighter.attack * crit;
  
  return power;
}

export function getBlockPower(fighter) {
  let dodgeChance = (Math.random() * (1)) + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}
