import { createFighterImage } from "../fighterPreview";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `Winner ${fighter.name}`,
    bodyElement: imageElement,
    onClose: () => {
      location.reload();
    }
  };
  
  showModal(modalElement);
}
